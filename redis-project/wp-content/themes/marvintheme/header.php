<?php
wp_head();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">

    <script src="https://www.google.com/recaptcha/api.js?render=6LfHZmwnAAAAAGAGRVZjYrVE-wwFuWewBksWHGZL"></script>
</head>



<body>
    <main id="main-content">
        <div id="primary">


            <!-- Topbar Start -->
            <?php get_template_part('template-parts/topbar'); ?>
            <!-- Topbar End -->


            <!-- Navbar Start -->
            <?php get_template_part('template-parts/navbar'); ?>

            <!-- Navbar End -->