<div id="home-product">
    <?php
    //section 1 home
    $homepageCache = new Redis_Cache_Class();
    $homeProduct1 = get_field('home-product-1', 'options');
    $homeKeyProduct1 = 'home-product-1';
    $homeProducts1 = $homepageCache->get_home_product($homeProduct1, $homeKeyProduct1);
    $homeProductBlock1 = Product_Class::product_block($homeProducts1);
    ?>

</div>
<?php

get_header();
?>

<!-- Products Start -->
<?php get_template_part('template-parts/product', 'block', ['product_type' => 'recent']); ?>
<div class="container-fluid pt-5 pb-3">
    <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span class="bg-secondary pr-3">Recent Products</span></h2>
    <div class="row px-xl-5">
        <?php
        echo $homeProductBlock1;
        ?>
    </div>
</div>
<!-- Products End -->



<!-- Back to Top -->
<a href="#" class="btn btn-primary back-to-top"><i class="fa fa-angle-double-up"></i></a>
<?php
get_footer();
