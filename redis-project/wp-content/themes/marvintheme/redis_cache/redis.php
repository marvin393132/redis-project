<?php
if (!class_exists("Redis_Cache_Class")) {
    class Redis_Cache_Class
    {
        public function __construct()
        {
            add_action('acf/options_page/save', [$this, 'theme_home_product_option_page'], 10, 2);
        }
        public function get_home_product($product_ids, $cacheKey)
        {
            global $wpdb;
            if (isset($cacheKey) || $cacheKey != null) {
                $products = wp_cache_get($cacheKey);

                if (!$products) {
                    $ids = '';
                    foreach ($product_ids as $product_id) {
                        $ids .= $product_id . ",";
                        $productKey = wp_cache_get("product-" . $product_id);
                        if (!$productKey) {
                            $pdListKey      = unserialize($productKey);
                            $pdListKey[]    = $cacheKey;
                            $pdListKey      = serialize($pdListKey);
                            wp_cache_set("product-" . $product_id, $pdListKey);
                        }
                        // $productKey[] = $cacheKey;
                        // $products = serialize($products);
                        // $products[] = wc_get_product($product_id);
                    }
                    $ids = substr($ids, 0, -1);
                    $sql = "SELECT p.ID, p.post_title, pm.meta_value as `regular_price`, pm2.meta_value as `sale_price` FROM `wp_posts` as p
                    INNER JOIN `wp_postmeta` as pm ON (p.ID = pm.post_id)
                    INNER JOIN `wp_postmeta` as pm2 ON (p.ID = pm2.post_id)
                    INNER JOIN `wp_postmeta` as pm3 ON (p.ID = pm3.post_id)
                    WHERE p.post_type = 'product' AND p.post_status = 'publish'
                    AND pm.meta_key = '_regular_price'
                    AND pm2.meta_key = '_sale_price'
                    AND p.ID IN ($ids)
                    GROUP BY p.ID";
                    $products = $wpdb->get_results($sql, ARRAY_A);
                    $products = serialize($products);
                    wp_cache_set($cacheKey, $products);
                }
                return $products;
            }
        }


        function theme_home_product_option_page($post_id, $menu_slug)
        {
            global $wpdb;
            if ('products-option' !== $menu_slug) {
                return;
            }

            $home_product_ids = get_field('home-product-1', $post_id);
            if ($home_product_ids) {
                $ids = '';
                foreach ($home_product_ids as $product_id) {
                    // $products[] = wc_get_product($product_id);
                    $ids .= $product_id . ",";
                    $productKey = wp_cache_get("product-" . $product_id);
                    $cacheKey   = 'home-product-1';
                    if (!$productKey) {
                        $pdListKey      = unserialize($productKey);
                        $pdListKey[]    = $cacheKey;
                        $pdListKey      = serialize($pdListKey);
                        wp_cache_set("product-" . $product_id, $pdListKey);
                    }
                }
                $ids = substr($ids, 0, -1);
                $sql = "SELECT p.ID, p.post_title, pm.meta_value as `regular_price`, pm2.meta_value as `sale_price` FROM `wp_posts` as p
                INNER JOIN `wp_postmeta` as pm ON (p.ID = pm.post_id)
                INNER JOIN `wp_postmeta` as pm2 ON (p.ID = pm2.post_id)
                INNER JOIN `wp_postmeta` as pm3 ON (p.ID = pm3.post_id)
                WHERE p.post_type = 'product' AND p.post_status = 'publish'
                AND pm.meta_key = '_regular_price'
                AND pm2.meta_key = '_sale_price'
                AND p.ID IN ($ids)
                GROUP BY p.ID";
                $products = $wpdb->get_results($sql, ARRAY_A);
                $products = serialize($products);
                wp_cache_set('home-product-1', $products);
            }
        }
    }
}
new Redis_Cache_Class();
