<?php

get_header();

if (isset($_GET['s'])) {
    $search = $_GET['s'];
    $searchResult = Product_Class::search_product($search);
}
?>
<!-- Products Start -->
<div class="container-fluid pt-5 pb-3">
    <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span class="bg-secondary pr-3">Recent Products</span></h2>
    <div class="row px-xl-5">
        <?php
        echo $searchResult;
        ?>
    </div>
</div>
<!-- Products End -->

<!-- Back to Top -->
<a href="#" class="btn btn-primary back-to-top"><i class="fa fa-angle-double-up"></i></a>
<?php
get_footer();
