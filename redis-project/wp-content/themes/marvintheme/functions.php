<?php
define("THEME_DIR", get_template_directory());

require THEME_DIR . '/redis_cache/redis.php';
require THEME_DIR . '/inc/product.php';

function mytheme_woocommerce_support()
{
    add_theme_support('woocommerce');
}
add_action('after_setup_theme', 'mytheme_woocommerce_support');

add_action('wp_enqueue_scripts', 'load_assets_func');

function load_assets_func()
{
    wp_enqueue_style('animate-min-css', get_template_directory_uri() . '/assets/lib/animate/animate.min.css');
    wp_enqueue_style('owl-carousel-min-css', get_template_directory_uri() . '/assets/lib/owlcarousel/assets/owl.carousel.min.css');
    wp_enqueue_style('fontawesome-css', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css');
    wp_enqueue_style('style-min-css', get_template_directory_uri() . '/assets/css/style.css');

    wp_dequeue_script('jquery');
    wp_enqueue_script('jquery-js', get_template_directory_uri() . '/assets/js/jquery/jquery.min.js');
    wp_enqueue_script('bootstrap-bundle-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js');
    wp_enqueue_script('easing-js',  get_template_directory_uri() . '/assets/lib/easing/easing.min.js');
    wp_enqueue_script('owl-carousel-js',  get_template_directory_uri() . '/assets/lib/owlcarousel/owl.carousel.min.js');
    wp_enqueue_script('main-js',  get_template_directory_uri() . '/assets/js/main.js');
}


// add_action('acf/options_page/save', 'my_acf_save_options_page', 10, 2);
// function my_acf_save_options_page($post_id, $menu_slug)
// {

//     if ('products-option' !== $menu_slug) {
//         return;
//     }
//     // Get newly saved values for the theme settings page.
//     $values = get_fields($post_id);

//     // Check the new value of a specific field.
//     $home_product_ids = get_field('home-product-1', $post_id);
//     if ($home_product_ids) {
//         foreach ($home_product_ids as $product_id) {
//             $products[] = wc_get_product($product_id);
//         }
//         // wp_cache_set('home-product-1', $products);
//     }
// }
// function clear_advert_main_transient()
// {
//     $screen = get_current_screen();
//     // if (strpos($screen->id, "acf-options-adverts") == true) {

//     // }
// }
// add_action('acf/save_post', 'clear_advert_main_transient', 20);
