<?php
class Product_Class
{
    public function __construct()
    {
        add_action("save_post_product", [$this, "save_store_product"], 10, 3);
    }

    public static function product_block($products)
    {
        $products = unserialize($products);
        if (is_array($products) && !empty($products)) :
            ob_start();
            foreach ($products as $product) :
                $productID      = $product['ID'];
                $thumbnail      = wp_get_attachment_image_src(get_post_thumbnail_id($productID), 'single-post-thumbnail')[0];
                $productName    = $product['post_title'];
                $salePrice      = $product['sale_price'];
                $regularPrice   = $product['regular_price'];
?>
                <div class="col-lg-3 col-md-4 col-sm-6 pb-1">
                    <div class="product-item bg-light mb-4">
                        <div class="product-img position-relative overflow-hidden">
                            <img class="img-fluid w-100" src="<?php echo $thumbnail; ?>" alt="">
                            <div class=" product-action">
                                <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-shopping-cart"></i></a>
                                <a class="btn btn-outline-dark btn-square" href=""><i class="far fa-heart"></i></a>
                                <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-sync-alt"></i></a>
                                <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-search"></i></a>
                            </div>
                        </div>
                        <div class="text-center py-4">
                            <a class="h6 text-decoration-none text-truncate" href=""><?php echo $productName; ?></a>
                            <div class="d-flex align-items-center justify-content-center mt-2">
                                <h5>$<?php echo $salePrice ?></h5>
                                <h6 class="text-muted ml-2"><del>$<?php echo $regularPrice; ?></del></h6>
                            </div>
                        </div>
                    </div>
                </div>
<?php
                $block = ob_get_contents();

            endforeach;
            ob_end_clean();
        endif;
        return $block;
    }


    public static function search_product($search)
    {
        global $wpdb;
        if (isset($search)) {
            $searchKey  = "search-key-" . $search;
            $sql = "SELECT p.ID, p.post_title, pm.meta_value as `regular_price`, pm2.meta_value as `sale_price` FROM `wp_posts` as p
            INNER JOIN `wp_postmeta` as pm ON (p.ID = pm.post_id)
            INNER JOIN `wp_postmeta` as pm2 ON (p.ID = pm2.post_id)
            INNER JOIN `wp_postmeta` as pm3 ON (p.ID = pm3.post_id)
            WHERE p.post_type = 'product' AND p.post_status = 'publish'
            AND pm.meta_key = '_regular_price'
            AND pm2.meta_key = '_sale_price'
            AND p.post_title LIKE '%$search%'
            GROUP BY p.ID";
            $products = $wpdb->get_results($sql, ARRAY_A);

            foreach ($products as $product) {
                $product_id = $product['ID'];
                $productKey = wp_cache_get("product-" . $product_id);
                if ($productKey) {
                    $pdListKey      = unserialize($productKey);
                    $pdListKey[]    = $searchKey;
                    $pdListKey      = serialize($pdListKey);
                    wp_cache_set("product-" . $product_id, $pdListKey);
                }
            }

            $products = serialize($products);
            $productBlock = self::product_block($products);
            return $productBlock;
        }
    }

    public function save_store_product($product_id, $post, $update)
    {
        echo '<pre>';
        print_r($product_id);
        echo '</pre>';
        // die();
        // $searchKey  = "search-key-bean";
        // wp_cache_delete($searchKey, 'search-key');
    }
}


new Product_Class();
